import React, { useEffect } from "react"
import { navigate } from "gatsby"
import { Button } from "@material-ui/core"
import useTwilioVideo from "../hooks/use-twilio-video"
const VideoDisplay = ({ roomID }) => {
  const { state, startVideo, leaveRoom, videoRef } = useTwilioVideo()

  useEffect(() => {
    if (!state.token) {
      navigate("/", { state: { roomName: roomID } })
    }

    if (!state.room) {
      startVideo()
    }

    if (typeof window !== "undefined") {
      window.addEventListener("beforeunload", leaveRoom)
    }

    return () => {
      if (typeof window !== "undefined") {
        window.removeEventListener("beforeunload", leaveRoom)
      }
    }
  }, [state, roomID, startVideo, leaveRoom])

  return (
    <>
      <h1>room: {roomID}</h1>
      {state.room && (
        <Button
          color="primary"
          size="small"
          ariant="contained"
          onClick={leaveRoom}
        >
          leave room
        </Button>
      )}
      <div className="video-view">
        <div ref={videoRef} />
      </div>
    </>
  )
}

export default VideoDisplay
