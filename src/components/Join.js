import React, { useEffect, useState } from "react"
import {
  FormControl,
  InputLabel,
  Input,
  FormGroup,
  Paper,
  Button,
  Grid,
} from "@material-ui/core"
import useTwilioVideo from "../hooks/use-twilio-video"
import { navigate } from "gatsby"

const Join = ({ location }) => {
  const defaultRoom = location?.state?.roomName || ""

  const { state, getRoomToken } = useTwilioVideo()
  const [identity, setIdentity] = useState("")
  const [roomName, setRoomName] = useState(defaultRoom)

  useEffect(() => {
    if (state.token && state.roomName) {
      navigate(`/room/${state.roomName}`)
    }
  }, [state])

  const handleSubmit = e => {
    e.preventDefault()
    getRoomToken({ identity, roomName })
  }

  return (
    <>
      <h1>Start or Join </h1>
      {/* <pre>{JSON.stringify(state, null, 2)}</pre> */}
      <Grid container justify="center" style={{ marginTop: "1rem" }}>
        <Grid item xs={11} md={8} lg={4}>
          <Paper style={{ margin: "1rem 0", padding: "0 1rem" }}>
            <form onSubmit={handleSubmit}>
              <FormGroup>
                <FormControl className="start-form">
                  <InputLabel htmlFor="identity">Display Name</InputLabel>
                  <Input
                    id="identity"
                    aria-describedby="identity"
                    type="text"
                    value={identity}
                    onChange={e => setIdentity(e.target.value)}
                  />
                </FormControl>
                <br />
                <FormControl className="start-form">
                  <InputLabel htmlFor="roomName">
                    Room you want to join.
                  </InputLabel>
                  <Input
                    id="roomName"
                    aria-describedby="roomName"
                    type="text"
                    value={roomName}
                    onChange={e => setRoomName(e.target.value)}
                  />
                </FormControl>
                <Button
                  color="primary"
                  size="small"
                  ariant="contained"
                  type="submit"
                >
                  Join Chat room
                </Button>
              </FormGroup>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  )
}

export default Join
